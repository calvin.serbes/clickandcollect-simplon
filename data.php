
<?php
session_start();
try{
$dsn = "mysql:dbname=CAC;host=127.0.0.1;port=3306";
$user = 'toto';
$pass = 'mdp'; 
$pdo = new PDO($dsn, $user, $pass);
}catch(Exception $e){
    $host = '127.0.0.1';
    $db   = 'CAC';
    $user = 'root';
    $pass = ''; 
    $pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
};

function readInfo(){
    global $pdo;
    $req = $pdo->query("select * from information;");
    return $req->fetchAll();
};

function updateInfo($adresse, $telephone, $horaire){
    global $pdo;
    $req = $pdo->prepare("update information set adresse=?, telephone=?, horaire=?;");
    $req->execute([$adresse, $telephone, $horaire]);
};

function createproduits($nom, $imageURL, $prixAuKilo, $dispo){
    global $pdo;
    $req = $pdo->prepare('insert into produits ( nom, imageURL, prixAuKilo, dispo) values (?, ?, ?, ?);');
    $req->execute([$nom, $imageURL, $prixAuKilo, $dispo]);
};

function readallproduits() {
    global $pdo;
    $req = $pdo->query("select * from produits;");
    return $req->fetchAll();
};

function readproduitsbyid($id) {
    global $pdo;
    $req = $pdo->prepare("select * from produits where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function updateproduits($id, $nom, $imageURL, $prixAuKilo, $dispo) {
    global $pdo;
    $req = $pdo->prepare('update produits set nom=?, imageURL=?, prixAuKilo=?,dispo=? where id=?;');
    $req->execute([$nom, $imageURL, $prixAuKilo, $dispo,$id]);
};

// function createcommande($id_client){
//     global $pdo;
//     $req = $pdo->prepare("insert into commande_a_etat (id_client") values ("id_client");

// };

function readallcommande(){
    global $pdo;
    $req = $pdo->query("select * from commande_a_etat");
    return $req->fetchAll();
};

function readcommandebyidclient($id_client){
    global $pdo;
    $req = $pdo->query("select * from commande_a_etat where id_client=?");
    $req->execute([$id_client]);
    return $req->fetchAll();
};

function readetatcommande($etat){
    global $pdo;
    $req = $pdo->query("select etat from commande_a_etat where id =?");
    $req->execute([$etat]);
    return $req->fetchAll();
};

function updateetatcommande($etat){
    global $pdo;
    $req = $pdo->prepare("update commande_a_etat set etat = ? where id =?");
    $req->execute([$etat]);
};
?>
