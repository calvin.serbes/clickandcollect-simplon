<link rel="stylesheet" href="style-index.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

<?php
    // include_once inclu qu'une seul fois
    include_once "../data.php";
    include "../homepage/navbar.php"; 

    $information = readInfo();

?>

    <form action="update-info.php" method="post">
    
        <label for="">Adresse :</label>
        <input type="text" name="adresse" id="" value="<?= $information[0]['adresse'];?>">

        <label for="">Téléphone :</label>
        <input type="text" name="telephone" id="" value="<?= $information[0]['telephone'];?>">

        <label for="">Horaires :</label>
        <input type="text" name="horaire" id="" value="<?= $information[0]['horaire'];?>">

        <input type="submit" value="Envoyer">
        
    
    </form>

    <?php include "../homepage/footer.php" ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
