<?php include_once"../data.php" ?>
<nav class="navbar navbar-dark bg-dark fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="../homepage/index.php">La Ferme Cool</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="offcanvas offcanvas-end text-bg-dark" tabindex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
      <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasDarkNavbarLabel">La Ferme Cool</h5>
        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      </div>
      <div class="offcanvas-body">
        <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="../homepage/index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../produits/viewproduits.php">Nos Produits</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Commande</a>
          </li>
          <?php if ($_SESSION["admin"]){?>
          <li id="btnadmin"class="nav-item">
            <a class="adm" href="../homepage/form-info.php">Modifier infos</a>
            <a class="adm" href="../produits/ajoutproduits.php">Ajouter produits</a>
            <a class="adm" href="../admin/logoutadmin.php">Deconnection</a>
          </li>
          <?php } ?>          
        </ul>
      </div>
    </div>
  </div>
</nav>