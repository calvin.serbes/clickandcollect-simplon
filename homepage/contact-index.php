<?php

    include_once "../debug.php";
    include_once "../data.php";
    $info = readInfo();
    
?>

<div class="container-contact-index">
    <div class="contact-index">

        <p> 
            <span class="info-gras">Adresse : </span> <?= $info[0]['adresse']?>            
        </p>
        <p>
            <span class="info-gras">Téléphone : </span><?= $info[0]['telephone']?>
        </p>
        <p>
            <span class="info-gras">Horaires : </span><?= $info[0]['horaire']?>
        </p>
        
    </div>
</div>