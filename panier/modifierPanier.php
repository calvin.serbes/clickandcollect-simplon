<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
</head>
<body>
    <?php include_once 'pdo.php'?>
    <?php include_once 'data.php'?>
    <form method="post" action="updatePanier.php?id=<?=$id?>">
        <?php $total = 0 ?>
        <?php for($i=0; $i < count($panier); $i++){?>
            <p><?php echo $panier[$i]['nom']; ?>
            Quantité : <input type="number" size=1 name="quantité" id="quantité" min="0" value="<?php echo $panier[$i]['quantité']; ?>">
            Prix au kilogramme : <?php echo $panier[$i]['prix']; ?>€
            <button>Supprimer</button></p>
        <?php } ?>
        <input type="submit" value="Valider">
    </form>
</body>
</html>