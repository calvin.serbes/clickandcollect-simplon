<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
</head>
<body>
    <?php include_once 'pdo.php'?>
    <?php include_once '.../data.php'?>
    <?php include_once '.../debug.php'?>
    
    <?php $produits = readallproduits(); ?>
    
    <form action="post">
        <?php $total = 0 ?>
        <?php for($i=0; $i < count($produits); $i++){?>
            <p><?php echo $produits[$i]['nom']; ?>
            Quantité : <?php //echo $produits[$i]['quantité']; ?>
            Prix au kilogramme : <?php echo $produits[$i]['prixAuKilo']; ?>€
            Total : <?php //echo $produits[$i]['quantité'] * $produits[$i]['prix']; ?>€</p>
            <?php //$total += $produits[$i]['quantité'] * $produits[$i]['prix']; ?>
        <?php } ?>
        <p>Prix total : <?php //echo $total ?>€</p>
        <label for="nom">Nom :
            <input type="text" id="nom">
        </label>
        <label for="email">Email :
            <input type="email" name="email" id="email">
        </label>
        <label for="tel">Téléphone :
            <input type="tel" name="tel" id="tel">
        </label>
        <button>Valider</button>
    </form>
    <button type="button" onclick="location.href='modifierPanier.php'">Modifier panier</button>
</body>
</html>