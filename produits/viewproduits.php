<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<link rel="stylesheet" href="../homepage/style-index.css">
<?php 
    include_once "../data.php";
    include "../homepage/navbar.php"; 
?>

<?php if ($_SESSION["admin"]){?>
<a href="../admin/logoutadmin.php">se deconnecter</a>
<?php } ?>
<?php 

$produits = readallproduits();


for($i=0;$i<count($produits);$i++) {?>
    <form method="post" action="../panier/updatePanier.php">
        <img src="<?= $produits[$i]['imageURL'] ?>">
        <h2><?=$produits[$i]['nom'] ?></h2>
        <p><?=$produits[$i]['prixAuKilo']?> €</p>
        <p><?=$produits[$i]['dispo']?></p>
        <?php 
            if ($_SESSION["admin"]){?>
                <div>
                    
                    <a href="./modifproduits.php?id=<?= $produits[$i]['id'] ?>">Modifier</a>
                </div>
        <?php
        }
        ?>
        <p>Quantité : <input type="number" name="qt" min="0" size="1"></p>
        <input type="hidden" name="id_prod" value="<?=$produits[$i]['id']?>">
        <input type="hidden" name="id_panier" value="??????">
        <button>Ajouter au panier</button>
    </form>

<?php } ?>
<button onclick="location.href='../panier/validerPanier.php'">Valider mon panier</button>

<!-- script cdn js bootstrap -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>