<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
<link href="../homepage/style-index.css" rel="stylesheet">
<?php 
include_once "../data.php";
include "../homepage/navbar.php";
$id = $_GET["id"];
$produits = readproduitsbyid($id)[0];
?>
<form action="updateproduits.php?id=<?=$id?>" method="post">
    <div>
        <label for="nom">Nom</label>
        <input type="text" name="nom" value="<?=$produits["nom"];?>">
    </div>
    <div>
        <label for="imageURL">URL de l'image</label>
        <input type="text" name="imageURL" value="<?=$produits["imageURL"];?>">
    </div><div>
        <label for="prixAuKilo">Prix au kilogramme</label>
        <input type="float" name="prixAuKilo" value="<?=$produits["prixAuKilo"];?>">
    </div>
    <div>
        <label for="dispo">Disponibilité</label>
        <input type="checkbox" name="dispo">
    </div>
    <input type="submit" value="continuer">
</form>
<?php
    include "../homepage/footer.php"
?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
