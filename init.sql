
DROP database if exists CAC;

create database CAC;
use CAC;

create table information(
    adresse varchar(800) not null,
    telephone varchar(800) not null,
    horaire varchar(800) not null
);

create table produits(
    id int not null auto_increment primary key,
    nom varchar(255) not null,
    imageURL varchar(800) not null,
    prixAuKilo float not null,
    dispo boolean 
);

create table commande_a_etat(
    id int not null auto_increment primary key,
    id_client int,
    etat enum('panier', 'valide', 'prete', 'recupere') default 'panier'
    -- foreign key (id_client) references client(id) on delete cascade,
);

INSERT INTO information (adresse, telephone, horaire) VALUES 
(
 '9301 Wilshire Blvd.
  Suite 504
  Beverly Hills, CA 90210-6149
  États-Unis','(310) 620-2187', 'du mardi au vendredi, de 09:00 a 16:00'
);

